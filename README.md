# Yugioh Anime Decks

A collection of Yugioh decks that try to emulate the feeling of duels in the anime.

## Project Goals
The decks are created with the following goals:
- The decks are meant to be competitive with one another but not competitive with typical Yugioh decks.
- The decks are meant to be more accessible than the average Yugioh deck.  Typically, complicated card text and large extra decks contribute to accessibility issues.  While some of this is unavoidable in a game like Yugioh, it is best to minimize it.
- While decks should have cards that combo off of one another, it is possible to have this happen too much.  In extreme cases, it can feel more like one player is playing "solitaire" than Yugioh and can continue for 10 minutes at a time.  This excessive level of interaction should be avoided at all costs.
- Cards should be repeated as seldom as possible to ensure duels feel as different as possible.
- One copy of each "banned" card is allowed in each deck.  Having hard-hitting cards like this make it feel more like the anime.
- Decks are intended to follow a particular theme.  This is typically based on a character but can cover other more general topics as well.
- While the rules are important to the game in general, there are no hard rules when it comes to building decks.  It is more important to build a fun deck than to follow arbitrary rules.
- These decks are somewhat reminiscent of "Goat Format" in spirit, but it places no limits on new cards.

## Future Updates
- Develop new decks.
- Make it easy to track win/loss/draw records between the decks.
- Improve balance between the decks.
- Add new cards the compliment decks when they are released.

## Special Thanks
A special thanks to Troy ([@TroyWD98 on Twitter](https://twitter.com/TroyWD98)) for coming up with this initial concept and building the first 11 decks.  I plan on updating those with his changes rather than making my own tweaks to them.